#include <stdio.h>
#include <stdlib.h>

const double PI = 3.14159;

double detCirculo(double raio){
    return PI*pow(raio, 2);
}

void redirecionadorDeCalculo(int op){
    double aux = 0;
    double raio = 0;

    switch(op){
        //1-circulo
        case 1 :
            printf("informe o raio do circulo: ");
            scanf("%lf", &raio);
            aux = detCirculo(raio);
            printf("a area do circulo de raio %.2f eh: %.2f", raio, aux);
        break;

        //2-cilindro
        case 2 :
            printf("cilindro");
        break;

        //3-cubo
        case 3 :
            printf("cubo");
        break;

        //4-piramede
        case 4 :
            printf("piramede");
        break;

        default:
            printf("invalido");
    }
}

int main()
{
    char ext;
    int op;
    printf("qual forma deseja detalhar?\n");
    printf("1-circulo 2-cilindro 3-cubo 4-piramede\n");
    scanf("%d", &op);

    redirecionadorDeCalculo(op);

    scanf("%c", ext);
    scanf("%c", ext);

    return 0;
}
